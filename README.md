# ASP Calculator
![ASP Calculator](./screenshot.jpg) 

## Description
This project is a web based 4-function calculator developed in ASP.  It uses cookies to remember the numbers and math functions selected by the user.  

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
This project can be viewed online [here](http://www.cavaliere.me/sites/webcalculator.asp).

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 17 March 2001.
>
> Download: [Version 1.0.0](/uploads/934fce1b0f536fd3bd25b576b932702a/aspwebcalculator.zip)

