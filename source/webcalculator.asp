<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ASP Calculator</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

<body>

<%
' --- Declare variables
  dim ans, sign, firstnum

' --- Retrieve previous values for variables (if any) from form.
  if (Len(Request.Cookies("ans")) > 0) then
    ans = CSng(Request.Cookies("ans"))
  else
    ans = 0
  end if
  if (Len(Request.Cookies("sign")) > 0) then
    sign = CStr(Request.Cookies("sign"))
  else
    sign=""
  end if
  if (Len(Request.Cookies("firstnum")) > 0) then
    firstnum = CStr(Request.Cookies("firstnum"))
  else
    firstnum = ""
  end if

' --- Figure out which button was pressed and execute corresponding action.
' -- Code for CLR button
  if (Request.Form("button") = "clear") then
    ans = 0
    sign=""
    firstnum = ""
  end if

' --- Code for buttons 0 through 9 and "."
  if ((Request.Form("button") = "1") or (Request.Form("button") = "2") or (Request.Form("button") = "3") or (Request.Form("button") = "4") or (Request.Form("button") = "5") or (Request.Form("button") = "6") or (Request.Form("button") = "7") or (Request.Form("button") = "8") or (Request.Form("button") = "9") or (Request.Form("button") = "0") or (Request.Form("button") = ".")) then
    if (firstnum = "0") then
      firstnum = CStr(Request.Form("button"))
    else
      firstnum = (firstnum & CStr((Request.Form("button"))))
    end if
    if (sign = "=") then
      sign = ""
      ans = 0
    end if
  end if

' --- Code for +, -, *, and / buttons
  if ((Request.Form("button") = "+") or (Request.Form("button") = "-") or (Request.Form("button") = "/") or (Request.Form("button") = "*") or (Request.Form("button") = "=")) then
    if (sign = "") then
      sign = Request.Form("button")
      if (firstnum <> "") then
        ans = CSng(firstnum)
      else
        ans = 0
      end if
      firstnum = ""
    else ' do previous sign
      if (sign ="+") then
        ans = add(firstnum, ans)
      end if
      if (sign ="-") then
        ans = subt(firstnum, ans)
      end if
      if (sign ="*") then
        ans = mult(firstnum, ans)
      end if
      if (sign ="/") then
        ans = div(firstnum, ans)
      end if
      sign = Request.Form("button")
      firstnum = ""
    end if
  end if

  Response.Cookies("ans") = ans
  Response.Cookies("firstnum")= firstnum
  Response.Cookies("sign") = sign
    
' --- Print the calculator with the current answer into the web browser and store
'     values for variables in the form.
%>

<TABLE BORDER=1 ALIGN="CENTER">
  <TR>
    <TD COLSPAN=4>
      <FORM>
        <%if (Len(firstnum) = 0) then%>
          <INPUT TYPE="TEXT" VALUE="<%=ans%>" onFocus="this.blur()">
        <%else%>
          <INPUT TYPE="TEXT" VALUE="<%=firstnum%>" onFocus="this.blur()">
        <%end if%>
      </FORM>
    </TD>
  </TR>
  <TR>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="clear" NAME="button">
        <INPUT TYPE="Submit" VALUE="CLR">
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="/" NAME="button">
        <INPUT TYPE="Submit" VALUE="/" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="*" NAME="button">
        <INPUT TYPE="Submit" VALUE="*" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="-" NAME="button">
        <INPUT TYPE="Submit" VALUE="-" WIDTH=35>
      </FORM>
    </TD>
  </TR>
  <TR>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="7" NAME="button">
        <INPUT TYPE="Submit" VALUE="7" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="8" NAME="button">
        <INPUT TYPE="Submit" VALUE="8" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="9" NAME="button">
        <INPUT TYPE="Submit" VALUE="9" WIDTH=35>
      </FORM>
    </TD>
    <TD ROWSPAN=2>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="+" NAME="button">
        <INPUT TYPE="Submit" VALUE="+" WIDTH=35 HEIGHT=70>
      </FORM>
    </TD>
  </TR>
  <TR>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="4" NAME="button">
        <INPUT TYPE="Submit" VALUE="4" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="5" NAME="button">
        <INPUT TYPE="Submit" VALUE="5" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="6" NAME="button">
        <INPUT TYPE="Submit" VALUE="6" WIDTH=35>
      </FORM>
    </TD>
  </TR>
  <TR>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="1" NAME="button">
        <INPUT TYPE="Submit" VALUE="1" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="2" NAME="button">
        <INPUT TYPE="Submit" VALUE="2" WIDTH=35>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="3" NAME="button">
        <INPUT TYPE="Submit" VALUE="3" WIDTH=35>
      </FORM>
    </TD>
    <TD ROWSPAN=2>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="=" NAME="button">
        <INPUT TYPE="Submit" VALUE="=" WIDTH=35 HEIGHT=70>
      </FORM>
    </TD>
  </TR>
  <TR>
    <TD COLSPAN=2>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="0" NAME="button">
        <INPUT TYPE="Submit" VALUE="0" WIDTH=80>
      </FORM>
    </TD>
    <TD>
      <FORM METHOD="POST" ACTION="webcalculator.asp">
        <INPUT TYPE="HIDDEN" VALUE="." NAME="button">
        <INPUT TYPE="Submit" VALUE="." WIDTH=35>
      </FORM>
    </TD>
</TABLE>

<% 
Function add(firstnum1, ans1)
  add = ans1 + firstnum1
End Function

Function subt(firstnum1, ans1)
  subt = ans1 - firstnum1
End Function

Function mult(firstnum1, ans1)
  mult = ans1 * firstnum1
End Function

Function div(firstnum1, ans1)
  if (firstnum1 = 0) then ' if division by zero then print error message.
    Response.Write ("ERROR:  division by zero.  Answer of 0 is being given for undefined.")
    div = 0
  else
    div = ans1 / firstnum1
  end if
End Function

Sub clearcalc ()
  Response.Cookies("ans") = 0
  Response.Cookies("firstnum")= ""
  Response.Cookies("sign") = ""
End Sub

%>

</body>
</html>
